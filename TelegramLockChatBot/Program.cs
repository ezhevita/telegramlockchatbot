﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using File = System.IO.File;

namespace TelegramLockChatBot {
	internal static class Program {
		private const int CreatorID = 204723509;
		private static TelegramBotClient Bot;
		private static int BotID;
		private static Dictionary<long, HashSet<int>> LockedChats;
		private static readonly SemaphoreSlim LogSemaphore = new SemaphoreSlim(1, 1);

		private static async void BotOnMessage(object sender, MessageEventArgs e) {
			Message message = e?.Message;
			if (message == null) {
				return;
			}

			string messageSender = message.From.FirstName + (string.IsNullOrEmpty(message.From.LastName) ? "" : " " + message.From.LastName) +
				(string.IsNullOrEmpty(message.From.Username) ? "" : $" (@{message.From.Username})");

			try {
				switch (message.Chat.Type) {
					case ChatType.Private:
						string[] args = message.Text.Split(' ');
						if ((message.Type == MessageType.Text) && (args[0] == "/start")) {
							await Bot.SendTextMessageAsync(
								message.Chat.Id,
								"Hello! I am a *chat locking bot*. If you don't want to delete your chat, but want to make it read-only, just add me there and type `/lock` (if you are a creator)",
								ParseMode.Markdown
							).ConfigureAwait(false);
							Log(
								$"Got private start message from {messageSender}"
							);

							break;
						}

						if ((message.Type == MessageType.Text) && (message.From.Id == CreatorID) && (args[0] == "/announce")) {
							await Bot.SendTextMessageAsync(CreatorID, "Starting...").ConfigureAwait(false);
							foreach ((long chatID, _) in LockedChats) {
								try {
									await Bot.SendTextMessageAsync(chatID, string.Join(' ', args.Skip(1))).ConfigureAwait(false);
								} catch {
									// ignored
								}
							}
						}

						break;
					case ChatType.Supergroup:
						List<ChatMember> admins = (await Bot.GetChatAdministratorsAsync(message.Chat.Id).ConfigureAwait(false)).ToList();
						if (admins.Select(x => x.User.Id).Contains(message.From.Id) && (message.Type == MessageType.Text)) {
							ChatMember botAdmin = admins.FirstOrDefault(x => x.User.Id == BotID);
							bool canRestrictMembers = (botAdmin != null) && botAdmin.CanRestrictMembers.GetValueOrDefault();
							bool canDeleteMessages = (botAdmin != null) && botAdmin.CanDeleteMessages.GetValueOrDefault();

							switch (message.Text.Split(new[] { ' ', '@' }, StringSplitOptions.RemoveEmptyEntries).First()) {
								case "/start":
									await Bot.SendTextMessageAsync(
										message.Chat.Id,
										"Hello! I am a *chat locking bot*. If you don't want to delete your chat, but want to make it read-only, just type `/lock` (if you are a creator and bot has `Ban users` and `Delete messages` permissions)",
										ParseMode.Markdown, replyToMessageId: message.MessageId
									).ConfigureAwait(false);
									Log($"Got start message from admin {messageSender}");

									return;
								case "/lock":
									if (!admins.Select(x => x.User.Id).Contains(BotID) || !canRestrictMembers || !canDeleteMessages) {
										await Bot.SendTextMessageAsync(
											message.Chat.Id,
											"Please grant me `Ban users` and `Delete messages` permission so I can lock this chat.",
											ParseMode.Markdown, replyToMessageId: message.MessageId
										).ConfigureAwait(false);
										Log(
											$"Got lock message from admin {messageSender}, but bot has not enough privileges"
										);

										return;
									}

									if (LockedChats.ContainsKey(message.Chat.Id)) {
										await Bot.SendTextMessageAsync(
											message.Chat.Id, "Chat is already locked.",
											replyToMessageId: message.MessageId
										).ConfigureAwait(false);
										Log($"Got lock message from admin {messageSender}, but chat is already locked");
									}

									LockedChats.Add(message.Chat.Id, new HashSet<int>());
									await File.WriteAllTextAsync("data.json", JsonConvert.SerializeObject(LockedChats)).ConfigureAwait(false);
									await Bot.SendTextMessageAsync(
										message.Chat.Id, "Chat is locked successfully!",
										replyToMessageId: message.MessageId
									).ConfigureAwait(false);
									Log(
										$"Chat {message.Chat.Title} is locked by {messageSender}"
									);

									return;
								case "/unlock":
									if (!admins.Select(x => x.User.Id).Contains(BotID) ||
										!canRestrictMembers) {
										await Bot.SendTextMessageAsync(
											message.Chat.Id,
											"Please grant me `Ban users` permissions so I can unlock this chat.",
											ParseMode.Markdown, replyToMessageId: message.MessageId
										).ConfigureAwait(false);
										Log(
											$"Got unlock message from admin {messageSender}, but bot has not enough privileges"
										);

										return;
									}

									if (!LockedChats.ContainsKey(message.Chat.Id)) {
										await Bot.SendTextMessageAsync(
											message.Chat.Id, "Chat is not locked.",
											replyToMessageId: message.MessageId
										).ConfigureAwait(false);
										Log($"Got unlock message from admin {messageSender}, but chat is not locked");

										return;
									}

									foreach (int user in LockedChats[message.Chat.Id]) {
										try {
											await Bot.RestrictChatMemberAsync(
												message.Chat.Id, user, new ChatPermissions {
													CanAddWebPagePreviews = true,
													CanSendMessages = true,
													CanSendMediaMessages = true,
													CanSendOtherMessages = true,
													CanSendPolls = true
												}
											).ConfigureAwait(false);
										} catch {
											// ignored
										}
									}

									LockedChats.Remove(message.Chat.Id);
									await File.WriteAllTextAsync("data.json", JsonConvert.SerializeObject(LockedChats)).ConfigureAwait(false);
									await Bot.SendTextMessageAsync(
										message.Chat.Id, "Successfully unlocked the chat!",
										replyToMessageId: message.MessageId
									).ConfigureAwait(false);
									Log($"Chat {message.Chat.Title} is unlocked by {messageSender}");

									return;
								default:
									return;
							}
						}

						if (!admins.Select(x => x.User.Id).Contains(BotID)) {
							LockedChats.Remove(message.Chat.Id);

							return;
						}

						if (LockedChats.ContainsKey(message.Chat.Id)) {
							LockedChats[message.Chat.Id].Add(message.From.Id);
							await File.WriteAllTextAsync("data.json", JsonConvert.SerializeObject(LockedChats)).ConfigureAwait(false);
							await Bot.RestrictChatMemberAsync(
								message.Chat.Id, message.From.Id, new ChatPermissions {
									CanSendMessages = false
								}
							).ConfigureAwait(false);
							await Bot.DeleteMessageAsync(message.Chat.Id, message.MessageId).ConfigureAwait(false);
							Log($"Restricted {messageSender} in chat {message.Chat.Title}");
						}

						return;
					case ChatType.Group:
						if (!(await Bot.GetChatAdministratorsAsync(message.Chat.Id).ConfigureAwait(false))
							.Select(x => x.User.Id).ToList()
							.Contains(message.From.Id)) {
							return;
						}

						await Bot.SendTextMessageAsync(
							message.Chat.Id, "Sorry, but bot only works with supergroups.",
							replyToMessageId: message.MessageId
						).ConfigureAwait(false);
						Log($"Got message not from supergroup {message.Chat.FirstName} by {messageSender}");

						return;
				}
			} catch (Exception ex) {
				Log($"Got an exception: {ex.Message}");
				Log($"Stacktrace: {ex.StackTrace}");
			}
		}

		private static void Log(string log) {
			log = $"[{DateTime.Now:G}] {log}";
			LogSemaphore.Wait();
			Console.WriteLine(log);
			File.AppendAllText("log.txt", log + Environment.NewLine, Encoding.Unicode);
			LogSemaphore.Release();
		}

		public static void Main() {
			if (File.Exists("log.txt")) {
				File.Delete("log.txt");
			}

			Log("Started TLCB");
			string token;
			if (File.Exists("token.txt")) {
				token = File.ReadAllText("token.txt");
			} else {
				Console.WriteLine("Please, enter your bot token:");
				token = Console.ReadLine();
			}

			LockedChats = File.Exists("data.json")
				? new Dictionary<long, HashSet<int>>(
					JsonConvert.DeserializeObject<Dictionary<long, HashSet<int>>>(File.ReadAllText("data.json"))
				)
				: new Dictionary<long, HashSet<int>>();
			Bot = new TelegramBotClient(token);
			User botUser = Bot.GetMeAsync().ConfigureAwait(false).GetAwaiter().GetResult();
			BotID = botUser.Id;
			string username = botUser.Username;
			Log($"Hello, I am @{username}!");
			Bot.OnMessage += BotOnMessage;
			Bot.StartReceiving();
			Thread.CurrentThread.Join();
		}
	}
}
